.. _messages:

Mensagens
=========

Visão Geral
-----------

Recursos de mensagem é a representação de uma mensagem que poderá ser
enviada pelo Cliente.

Mensagem
--------

Para se criar uma mensagem deve-se utilizar o seguinte endpoint:

::

    `POST /messages`

Para se listar todas as mensagens deve-se utilizar o seguinte endpoint:

::

    `GET /messages`

Para se listar uma mensagem deve-se utilizar o seguinte endpoint:

::

    `GET /messages/ID`

Status
------

+---------------------+--------------------------------+
| Status              | Descrição                      |
+=====================+================================+
| + CREATED           | Mensagem criada                |
+---------------------+--------------------------------+
| + SENT              | Mensagem enviada               |
+---------------------+--------------------------------+
| + DELIVERED         | Mensagem entregue              |
+---------------------+--------------------------------+
| + SYSTEM\_ERROR     | Erro de sistema                |
+---------------------+--------------------------------+
| + RECEIVER\_ERROR   | Erro do gateway                |
+---------------------+--------------------------------+
| + OPERATOR\_ERROR   | Erro da operadora telefonica   |
+---------------------+--------------------------------+

Criando mensagem
----------------

.. code:: http

    POST /messages HTTP/1.1
    Authorization: Bearer YourTokenComesHere
    Accept: application/json
    User-Agent: Http/2.2
    Host: HOST

    {
        "message": "This is a message",
        "from": "Something",
        "to": 554899246868,
        "type": "SMS",
    }

    A solicitação acima retorna a seguinte resposta:

.. code:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
      "createAt": "2016-06-17T18:21:23+00:00",
      "from": "Something",
      "id": 9,
      "message": "This is a message",
      "lastModifiedAt": "2016-06-17T18:21:23+00:00",
      "to": 554899246868,
      "type": "SMS"
    }

    Se a mensagem estiver incorreta o retorno será o seguinte:

.. code:: http

    HTTP/1.1 409 Conflict
    Content-Type: application/json

    {
        "error": "The message \"invalid\" needs to be between 10 and 150 chars.",
        "code": 409
    }

**MENSAGENS PARA PARAMENTROS INCORRETOS**

+-------------+---------------------------------------------------------------+
| Argumento   | Mensagem                                                      |
+=============+===============================================================+
| message     | The message "invalid" needs to be between 10 and 150 chars.   |
+-------------+---------------------------------------------------------------+
| from        | The name "%s" needs to be between 3 and 15 characters.        |
+-------------+---------------------------------------------------------------+
| to          | The number %s is a invalid number.                            |
+-------------+---------------------------------------------------------------+
| type        | The number %s is a invalid number.                            |
+-------------+---------------------------------------------------------------+

O Recurso de Mensagem é composto pelos seguintes atributos

+----------------+---------------------------------------+
| Atributo       | Descrição                             |
+================+=======================================+
| + id           | Representa uma ID                     |
+----------------+---------------------------------------+
| + createdAt    | Representa a data de criação          |
+----------------+---------------------------------------+
| + modifiedAt   | Representa a data de modificação      |
+----------------+---------------------------------------+
| + message      | Representa o texto da mensagem        |
+----------------+---------------------------------------+
| + owner        | Representa uma account                |
+----------------+---------------------------------------+
| + from         | Representa o remetente                |
+----------------+---------------------------------------+
| + to           | Representa o numero do destinatario   |
+----------------+---------------------------------------+
| + type         | Representa o tipo de mensagem         |
+----------------+---------------------------------------+

**PARÂMETROS DO PAYLOAD**

+-------------+---------------+--------------------------------------+
| Argumento   | Obrigatório   | Observações                          |
+=============+===============+======================================+
| message     | Sim           | Mensagem entre 10 e 150 caracteres   |
+-------------+---------------+--------------------------------------+
| from        | Sim           | Nome entre 3-15 caracteres           |
+-------------+---------------+--------------------------------------+
| to          | Sim           | Apenas numeros inteiros              |
+-------------+---------------+--------------------------------------+
| type        | Sim           | Tipo de mensagem                     |
+-------------+---------------+--------------------------------------+

Response
^^^^^^^^

+----------------+--------------------------------+
| Atributo       | Descrição                      |
+================+================================+
| + id           | Novo ID                        |
+----------------+--------------------------------+
| + createdAt    | DateTime atual                 |
+----------------+--------------------------------+
| + modifiedAt   | DateTime atual                 |
+----------------+--------------------------------+
| + message      | A mesma usada na solicitação   |
+----------------+--------------------------------+
| + from         | A mesma usada na solicitação   |
+----------------+--------------------------------+
| + to           | A mesma usada na solicitação   |
+----------------+--------------------------------+
| + type         | A mesma usada na solicitação   |
+----------------+--------------------------------+

**PARÂMETROS DO PAYLOAD**

+-------------+---------------+----------------+
| Argumento   | Obrigatório   | Observações    |
+=============+===============+================+
| ID          | Sim           | Unique e int   |
+-------------+---------------+----------------+

Listando todas as menssagens
----------------------------

.. code:: http

    GET /messages HTTP/1.1
    Authorization: Bearer YourTokenComesHere
    Accept: application/json
    Content-Type: application/json

    {
        "count": 2,
        "items": [
            {
              "createAt": "2016-06-17T18:21:23+00:00",
              "from": "Something",
              "id": 9,
              "message": "This is a message",
              "lastModifiedAt": "2016-06-17T18:21:23+00:00",
              "to": 554899246868,
              "type": "SMS"
            },
            {
              "createAt": "2016-06-17T18:52:31+00:00",
              "from": "Something",
              "id": 10,
              "message": "This is a message",
              "lastModifiedAt": "2016-06-17T18:52:31+00:00",
              "to": 554899999999,
              "type": "SMS"
            }
        ]
    }

    Se não houverem mensagens o retorno será o seguinte:

.. code:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "error": "You don't have any Message yet.",
        "code": 404
    }

**PARÂMETROS DO PAYLOAD**

+-------------+---------------+---------------------------------------------------------------+
| Argumento   | Obrigatório   | Observações                                                   |
+=============+===============+===============================================================+
| ID\_Owner   | Sim           | O ID irá ser gerado de acordo com a Account que está logada   |
+-------------+---------------+---------------------------------------------------------------+

Response
^^^^^^^^

+------------+---------------------+
| Atributo   | Descrição           |
+============+=====================+
| + count    | Contador            |
+------------+---------------------+
| + items    | Array de messages   |
+------------+---------------------+

Detalhes de uma Mensagem
------------------------

.. code:: http

    GET /messages/9 HTTP/1.1
    Authorization: Bearer YourTokenComesHere
    Accept: application/json
    Content-Type: application/json

    {
        "createAt": "2016-06-17T18:21:23+00:00",
        "from": "Something",
        "id": 9,
        "message": "This is a message",
        "lastModifiedAt": "2016-06-17T18:21:23+00:00",
        "to": 554899246868,
        "type": "SMS"
    }

    Se não houver nenhuma mensagem com esse ID o retorno será o
    seguinte:

.. code:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "error": "Message not found.",
        "code": 404
    }

**PARÂMETROS DO PAYLOAD**

+-------------+---------------+---------------------------------------------------------------+
| Argumento   | Obrigatório   | Observações                                                   |
+=============+===============+===============================================================+
| ID\_Owner   | Sim           | O ID irá ser gerado de acordo com a Account que está logada   |
+-------------+---------------+---------------------------------------------------------------+
| ID          | Sim           | ID da Message                                                 |
+-------------+---------------+---------------------------------------------------------------+

Response
^^^^^^^^

+----------------+---------------------------------+
| Atributo       | Descrição                       |
+================+=================================+
| + id           | Mesmo ID usado na solicitação   |
+----------------+---------------------------------+
| + createdAt    | Dado criado anteriormente       |
+----------------+---------------------------------+
| + modifiedAt   | Dado criado anteriormente       |
+----------------+---------------------------------+
| + message      | Dado criado anteriormente       |
+----------------+---------------------------------+
| + from         | Dado criado anteriormente       |
+----------------+---------------------------------+
| + to           | Dado criado anteriormente       |
+----------------+---------------------------------+
| + type         | Dado criado anteriormente       |
+----------------+---------------------------------+

Contents:
---------

Contents:
---------

* :ref:`authorization`
* :ref:`campaigns`
* :ref:`contacts`
* :ref:`groups`
* :ref:`http_status`
* :ref:`user_sumary`
