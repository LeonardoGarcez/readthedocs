.. _user_sumary:

GET /users/{id}/summary
=======================

-  Request (application/json)

   -  Headers

      ::

          Authorization: Bearer YourTokenComesHere
          Accept: application/json

   -  Body

      ::

          {
              "name": "Perry",
              "lastLogin": "2016-07-18T12:02:59-03:00",
              "summary": {
                  "product": "Plano SMS",
                  "messages": {
                      "total": 300,
                      "sent": 10
                  },
                  "contacts": {
                      "total": 230
                  },
                  "actions": {
                      "total": 10
                  },
                  "groups": {
                      "total": 25
                  }
              }
          }



Contents:
---------

* :ref:`authorization`
* :ref:`campaigns`
* :ref:`contacts`
* :ref:`groups`
* :ref:`http_status`
* :ref:`messages`
